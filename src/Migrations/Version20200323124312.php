<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323124312 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admission ADD patient_id INT NOT NULL, ADD chambre_id INT NOT NULL');
        $this->addSql('ALTER TABLE admission ADD CONSTRAINT FK_F4BB024A6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE admission ADD CONSTRAINT FK_F4BB024A9B177F54 FOREIGN KEY (chambre_id) REFERENCES chambre (id)');
        $this->addSql('CREATE INDEX IDX_F4BB024A6B899279 ON admission (patient_id)');
        $this->addSql('CREATE INDEX IDX_F4BB024A9B177F54 ON admission (chambre_id)');
        $this->addSql('ALTER TABLE chambre ADD hopital_id INT NOT NULL');
        $this->addSql('ALTER TABLE chambre ADD CONSTRAINT FK_C509E4FFCC0FBF92 FOREIGN KEY (hopital_id) REFERENCES hopital (id)');
        $this->addSql('CREATE INDEX IDX_C509E4FFCC0FBF92 ON chambre (hopital_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admission DROP FOREIGN KEY FK_F4BB024A6B899279');
        $this->addSql('ALTER TABLE admission DROP FOREIGN KEY FK_F4BB024A9B177F54');
        $this->addSql('DROP INDEX IDX_F4BB024A6B899279 ON admission');
        $this->addSql('DROP INDEX IDX_F4BB024A9B177F54 ON admission');
        $this->addSql('ALTER TABLE admission DROP patient_id, DROP chambre_id');
        $this->addSql('ALTER TABLE chambre DROP FOREIGN KEY FK_C509E4FFCC0FBF92');
        $this->addSql('DROP INDEX IDX_C509E4FFCC0FBF92 ON chambre');
        $this->addSql('ALTER TABLE chambre DROP hopital_id');
    }
}
