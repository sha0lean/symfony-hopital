<?php

namespace App\Controller;

use App\Entity\Admission;
use App\Entity\Patient;
use App\Form\PatientType;
use App\Form\AdmissionType;
use App\Repository\PatientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/patient")
 */
class PatientController extends AbstractController
{
    /**
     * @Route("/", name="patient_index", methods={"GET","POST"})
     */
    public function index(PatientRepository $patientRepository, Request $request): Response
    {
        $patient = new Patient();
        $form = $this->createForm(PatientType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($patient);
            $entityManager->flush();

            return $this->redirectToRoute('patient_index');
        }

        return $this->render('patient/index.html.twig', [
            'patients' => $patientRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    // fuck la methode new !!

    /**
     * @Route("/{id}", name="patient_show", methods={"GET","POST"})
     */
    public function show(Patient $patient, Request $request): Response
    {

        $formEditPatient = $this->createForm(PatientType::class, $patient);
        $formEditPatient->handleRequest($request);

        if ($formEditPatient->isSubmitted() && $formEditPatient->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('patient_index');
        }
// ------------------------------------------------------------------------------

        $admission = new Admission();

        $admission->setPatient($patient);      // je set le patient en cours

        $form = $this->createForm(AdmissionType::class, $admission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($admission);
            $entityManager->flush();

            return $this->redirectToRoute('admission_index');
        }

        return $this->render('patient/show.html.twig', [
            'patient' => $patient,
            'admission' => $admission,
            'form' => $form->createView(),
            'patientForm' => $formEditPatient->createView(),

        ]);
    }

    /**
     * @Route("/{id}/edit", name="patient_edit", methods={"GET","POST"})
     */
    // public function edit(Request $request, Patient $patient): Response
    // {
    //     $form = $this->createForm(PatientType::class, $patient);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->getDoctrine()->getManager()->flush();

    //         return $this->redirectToRoute('patient_index');
    //     }

    //     return $this->render('patient/edit.html.twig', [
    //         'patient' => $patient,
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * @Route("/{id}", name="patient_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Patient $patient): Response
    {
        if ($this->isCsrfTokenValid('delete'.$patient->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($patient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('patient_index');
    }
}
