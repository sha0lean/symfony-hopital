<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdmissionRepository")
 */
class Admission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_debut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_fin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="admissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chambre", inversedBy="admissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chambre;
// ---------------------------------------------------------------------------------------------------

    public function getId(): ?int
    {
        return $this->id;
    }
// ---------------------------------------------------------------------------------------------------

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;
        
        return $this;
    }
// ---------------------------------------------------------------------------------------------------
    public function __construct()
    {
        $this->setDateDebut = (new \DateTime('now'));
    }

// ---------------------------------------------------------------------------------------------------

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;
        
        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getPatient(): ?patient
    {
        return $this->patient;
    }

    public function setPatient(?patient $patient): self
    {
        $this->patient = $patient;
        
        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getChambre(): ?chambre
    {
        return $this->chambre;
    }

    public function setChambre(?chambre $chambre): self
    {
        $this->chambre = $chambre;
        
        return $this;
    }
// ---------------------------------------------------------------------------------------------------

}

