<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HopitalRepository")
 */
class Hopital
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $code_postale;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambre", mappedBy="hopital")
     */
    private $chambres;
// ---------------------------------------------------------------------------------------------------

    public function __construct()
    {
        $this->chambres = new ArrayCollection();
    }
// ---------------------------------------------------------------------------------------------------

    public function __toString()
    {
        return $this->getNom();
    }
// ---------------------------------------------------------------------------------------------------

    public function getId(): ?int
    {
        return $this->id;
    }
// ---------------------------------------------------------------------------------------------------

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getCodePostale(): ?string
    {
        return $this->code_postale;
    }

    public function setCodePostale(string $code_postale): self
    {
        $this->code_postale = $code_postale;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    /**
     * @return Collection|Chambre[]
     */
    public function getChambres(): Collection
    {
        return $this->chambres;
    }

    public function addChambre(Chambre $chambre): self
    {
        if (!$this->chambres->contains($chambre)) {
            $this->chambres[] = $chambre;
            $chambre->setHopital($this);
        }

        return $this;
    }

    public function removeChambre(Chambre $chambre): self
    {
        if ($this->chambres->contains($chambre)) {
            $this->chambres->removeElement($chambre);
            // set the owning side to null (unless already changed)
            if ($chambre->getHopital() === $this) {
                $chambre->setHopital(null);
            }
        }

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

}
