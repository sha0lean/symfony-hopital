<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $numero_avs;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="patient")
     */
    private $admissions;
// ---------------------------------------------------------------------------------------------------

    public function __construct()
    {
        $this->admissions = new ArrayCollection();
    }
// ---------------------------------------------------------------------------------------------------

    public function __toString()
    {
        return $this->getNom(). " ".$this->getPrenom();
    }
// ---------------------------------------------------------------------------------------------------

    public function getId(): ?int
    {
        return $this->id;
    }
// ---------------------------------------------------------------------------------------------------

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getNumeroAvs(): ?string
    {
        return $this->numero_avs;
    }

    public function setNumeroAvs(string $numero_avs): self
    {
        $this->numero_avs = $numero_avs;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

    /**
     * @return Collection|Admission[]
     */
    public function getAdmissions(): Collection
    {
        return $this->admissions;
    }

    public function addAdmission(Admission $admission): self
    {
        if (!$this->admissions->contains($admission)) {
            $this->admissions[] = $admission;
            $admission->setPatient($this);
        }

        return $this;
    }

    public function removeAdmission(Admission $admission): self
    {
        if ($this->admissions->contains($admission)) {
            $this->admissions->removeElement($admission);
            // set the owning side to null (unless already changed)
            if ($admission->getPatient() === $this) {
                $admission->setPatient(null);
            }
        }

        return $this;
    }
// ---------------------------------------------------------------------------------------------------

}
