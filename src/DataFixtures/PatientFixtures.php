<?php

namespace App\DataFixtures;

use App\Entity\Patient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PatientFixtures extends Fixture //implements DependentFixtureInterface // ATTENTION CREE CE QU'IL FAUT AVANT !!!!!
{
    public function load(ObjectManager $manager)
    {
        
        $infos_malades = [
            
            ["LIGEARD",  "Remy",      "123.4567.8910.13",  "30-06-1995"],
            ["DANGER",   "Abella",    "123.4567.8910.13",  "20-04-1995"],
            ["WILLIS",   "Emilly",    "123.4567.8910.13",  "10-06-1996"],
            ["QUINN",    "Kyler",     "123.4567.8910.13",  "28-06-1992"],
            ["FERRARA",  "Manuel",    "123.4567.8910.13",  "03-03-1985"],
            ["KHODL",    "Nathanael", "123.4567.8910.13",  "01-04-1993"],
            ["TABET",    "Hassinah",  "123.4567.8910.13",  "11-09-2011"],
            ["PONS",     "Roxanne",   "123.4567.8910.13",  "15-03-1997"],
            ["BEL",      "Hugo",      "123.4567.8910.13",  "03-03-1995"],
            ["BERTON",   "Maxime",    "123.4567.8910.13",  "15-04-1995"],
            
        ];

        foreach ($infos_malades as $infos_malade){
   
            $patient = new Patient();

            $patient
                    ->setNom($infos_malade[0])
                    ->setPrenom($infos_malade[1])
                    ->setNumeroAvs($infos_malade[2])
                    ->setDateNaissance(new \DateTime($infos_malade[3]));

            $manager->persist($patient); // "commit"
        }  
        $manager->flush(); // "push" to db
    }
}
