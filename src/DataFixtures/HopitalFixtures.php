<?php

namespace App\DataFixtures;

use App\Entity\Hopital;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class HopitalFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $hostos = [
            
            ["Clinique de Joli-Mont",     "Avenue Trembley 45",          "1209",  "Genève",        "CH"],
            ["Hôpital Beau-Séjour",       "Rue Alcide-Jentzer 17",       "1205",  "Genève",        "CH"],
            ["Hôpital de Psychiatrie",    "Chemin du Petit-Bel-Air 2,",  "1226",  "Thônex",        "CH"],
            ["Hôpital des Trois-Chêne",   "Route de Mon-Idée",           "1226",  "Thônex",        "CH"],
            ["Clinique de Crans-Montana", "Impasse Clairmont 2",         "3963",  "Crans-Montana", "CH"],
            
        ];

        foreach ($hostos as $hosto){
   
            $hopital = new Hopital();

            $hopital
                    ->setNom($hosto[0])
                    ->setAdresse($hosto[1])
                    ->setCodePostale($hosto[2])
                    ->setVille($hosto[3])
                    ->setPays($hosto[4]);

            $manager->persist($hopital); // "commit"
        }   
        $manager->flush(); // "push" to db
    }
}
