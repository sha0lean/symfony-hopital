<?php

namespace App\DataFixtures;

use App\entity\Chambre; 
use App\Repository\HopitalRepository; // va chercher dans repo ce que t'as besoin
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\DependentFixtureInterface; 
// ATTENTION CREE CE QU'IL FAUT AVANT !!!!!        ↓ ↑

class ChambreFixtures extends Fixture implements DependentFixtureInterface
{
    private $hopitalRepository; 

    public function __construct(HopitalRepository $hopitalRepository)
    {
        $this->hopitalRepository = $hopitalRepository;
    }

    public function load(ObjectManager $manager)
    {
   
        $hopitals = $this->hopitalRepository->findAll();

        foreach($hopitals as $hopital){
            $chambreToCreate = rand(3,8); 
           
            for($i =1; $i<= $chambreToCreate; $i++){
                $chambre = new Chambre(); 
                $chambre->setNom("Chambre no* : ".(rand(100, 1000))); 
                $chambre->setHopital($hopital); 
                $chambre->setCapacite(rand(100,1000));
              
                $manager->persist($chambre);
            }
        }
        $manager->flush();
        
    }
    public function getDependencies()
    {
        return [
            HopitalFixtures::class
        ];
    }
}